************************
* Annuaire Intelligent *
************************

Par Carolina CATAN, Baptiste MOUCHARD et Alexis ARRIAL

Introduction :

Ce logiciel a pour but de permettre à l'utilisateur de pouvoir créer un annuaire de stagiaire ordonné par ordre alphabétique en suivant le nom de famille.
Chaque entrée de stagiaire peut être accompagné d'information complémentaire qui sont le prénom, le département de résidence, le nom de la promotion que le stagiaire a suivi et enfin l'année d'inscription à cette formation.
Ainsi, l'utilisateur peut se créer une base de données de stagiaires avec la possibilité d'effectuer des recherches précises de stagiaire et d'exporter les données désirées.

Fonctionnalité :

Importer une liste de stagiaire :

Cette fonctionnalité permet à l'utilisateur de pouvoir importer une liste de stagiaire issu d'un fichier ".txt". Cependant, quelques règles de formatage des stagiaires, présent dans la liste importée sous format « .txt », sont à respecter. Cette fonction s’effectue en cliquant sur le bouton « Importer ». Chaque stagiaire devra être sous le format ci-dessous :

NOM
Prénom
Département -> résidence du stagiaire
Promotion
Année -> année d’entrée dans la formation
*

Chaque champ aura une taille limite qui est de :

Nom -> 30 caractères
Prénom -> 30 caractères
Département -> 4 caractères
Promotion -> 15 caractères
Année -> 5 caractères

Une fois le fichier sélectionné, chaque stagiaire sera ajouté à la base de données qui sera conservé dans le temps.

Session administrateur :

La session administrateur permet l’accès aux fonctionnalités « Modifier » et « Supprimer ». Pour cela, il faut utiliser le mot de passe administrateur dans le champ « Mot de passe » puis cliquer sur le bouton « Se Connecter ». Si l’utilisateur ne souhaite plus utiliser les fonctionnalités administrateur, il suffit de cliquer sur le bouton « Se déconnecter ».

Afficher l’intégralité de la base de données :

Il est possible d’afficher l’intégralité de la base de données par ordre alphabétique en fonction du nom de famille. Pour cela, le bouton « Tout voir » permet de le faire.

Ajouter un stagiaire à la base de données :

Grace au bouton « Ajouter », il est possible d’ajouter un stagiaire à la base de données. Pour cela, il est nécessaire de remplir le champ Nom au minimum. Les autres champs (Prénom, Département, Promotion et Année) ne sont pas nécessaires à l’ajout du stagiaire dans la base de données. Ces informations optionnelles peuvent être ajouter plus tard grâce à la fonction « Modifier ».

Rechercher un stagiaire :

En utilisant le bouton « Rechercher », il est possible de retrouver un stagiaire ou plusieurs stagiaires dans la base de données en fonction du ou des critères renseignés dans les champs Nom, Prénom, Département, Promotion et Année.

Modifier un stagiaire :

En cas d’erreur de saisie ou de changement de situation d’un stagiaire, il est possible de le modifier en sélectionnant le stagiaire à modifier dans la liste. Les données du stagiaire sélectionné s’afficheront dans les champs et pourront être ainsi modifiées par l’utilisateur puis le rendre effectif en cliquant sur le bouton « Modifier ». Pour rappel, cette fonctionnalité est accessible uniquement depuis une session administrateur.

Supprimer un stagiaire :

Il est possible de supprimer un stagiaire de la base de données. Pour cela, après avoir recherché le stagiaire à supprimer, il suffit de le sélectionner dans la liste (les champs vont se remplir avec les informations du stagiaire sélectionné) puis en cliquant sur le bouton « Supprimer », le stagiaire sera délété de la base de données. Pour rappel, cette fonctionnalité est accessible uniquement depuis une session administrateur.

Exporter une liste de stagiaire :

Après avoir effectué une recherche, il est possible d’exporter la liste de stagiaire au format PDF. Pour cela, à la suite de la fonction « Rechercher » ou « Tout voir », il faut cliquer sur le bouton « Exporter ». 
