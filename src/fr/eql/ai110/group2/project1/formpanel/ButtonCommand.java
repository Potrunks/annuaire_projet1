package fr.eql.ai110.group2.project1.formpanel;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

public class ButtonCommand extends GridPane {
	
	private Button btnSearch = new Button("Rechercher");
	private Button btnExport = new Button("Exporter");
	private Button btnModify = new Button("Modifier");
	private Button btnErase = new Button("Supprimer");
	private Button btnAdd = new Button("Ajouter");
	private Button btnShowAll = new Button("Tout voir");
	private Button btnReset = new Button("Réinitialiser");
	private Button btnEmpty = new Button();
	private FileChooserFirstLaunch fileChooserFirstLaunch = new FileChooserFirstLaunch();
	
	public ButtonCommand() {
		addRow(0, btnEmpty, btnModify);
		addRow(1, btnSearch, btnShowAll);
		addRow(2, btnAdd, btnErase);
		addRow(3, fileChooserFirstLaunch.getOpen(), btnExport);
		
		btnEmpty.setId("logo");
		btnSearch.setPrefSize(100, 60);
		btnExport.setPrefSize(100, 60);
		btnModify.setPrefSize(100, 60);
		btnErase.setPrefSize(100, 60);
		btnAdd.setPrefSize(100, 60);
		btnShowAll.setPrefSize(100, 60);
		
		setAlignment(Pos.CENTER);
		setVgap(20);
		setHgap(20);
		
	}

	public Button getBtnSearch() {
		return btnSearch;
	}

	public void setBtnSearch(Button btnSearch) {
		this.btnSearch = btnSearch;
	}

	public Button getBtnExport() {
		return btnExport;
	}

	public void setBtnExport(Button btnExport) {
		this.btnExport = btnExport;
	}

	public Button getBtnModify() {
		return btnModify;
	}

	public void setBtnModify(Button btnModify) {
		this.btnModify = btnModify;
	}

	public Button getBtnErase() {
		return btnErase;
	}

	public void setBtnErase(Button btnErase) {
		this.btnErase = btnErase;
	}

	public Button getBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(Button btnAdd) {
		this.btnAdd = btnAdd;
	}

	public Button getBtnShowAll() {
		return btnShowAll;
	}

	public void setBtnShowAll(Button btnShowAll) {
		this.btnShowAll = btnShowAll;
	}

	public Button getBtnReset() {
		return btnReset;
	}

	public void setBtnReset(Button btnReset) {
		this.btnReset = btnReset;
	}

	public FileChooserFirstLaunch getFileChooserFirstLaunch() {
		return fileChooserFirstLaunch;
	}

	public void setFileChooserFirstLaunch(FileChooserFirstLaunch fileChooserFirstLaunch) {
		this.fileChooserFirstLaunch = fileChooserFirstLaunch;
	}
	
}
