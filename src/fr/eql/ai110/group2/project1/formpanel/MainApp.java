package fr.eql.ai110.group2.project1.formpanel;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import fr.eql.ai110.group2.project1.database.DAO;
import fr.eql.ai110.group2.project1.database.Export;
import fr.eql.ai110.group2.project1.database.Student;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;

//La classe de lancement de l'application:
public class MainApp extends Application{

	private DAO dao = new DAO();
	private boolean isAdmin = false;
	private Export export = new Export();

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {		
		MainPanel root = new MainPanel();
		Scene scene = new Scene(root,1200,800);
		scene.getStylesheets().add(getClass().getResource("./NewFile.css").toExternalForm());
		stage.setTitle(" ABC Book - Annuaire Intelligent by Groupe 2");
		stage.setScene(scene);
		stage.sizeToScene();
		stage.show();


		root.getMainVBox().getBtnCo().getFileChooserFirstLaunch().getOpen().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				root.getMainVBox().getLabelInfo().setText(null);
				dao.launch(event, stage, root);
			}
		});

		root.getMainVBox().getBtnDoc().getBtnDoc().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {


				if ((new File("./README.txt")).exists()) {

					Process p = null;
					try {
						p = Runtime
								.getRuntime()
								.exec("rundll32 url.dll,FileProtocolHandler .\\README.txt");
					} catch (IOException e) {

						e.printStackTrace();
					}
					try {
						p.waitFor();
					} catch (InterruptedException e) {

						e.printStackTrace();
					}

				} else {

					System.out.println("File does not exist");

				}




			};
		}
				);

		root.getMainVBox().getBtnCo().getBtnShowAll().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				root.getMainVBox().getLabelInfo().setText(null);
				RandomAccessFile raf;
				try {
					raf = new RandomAccessFile(dao.getDestinationFilePath(), "rw");
					if (raf.length()!=0) {
						root.getTblvStudents().getObsStudent().clear();
						root.getTblvStudents().getObsStudent().addAll(dao.allBrowse(0));
					} else {
						root.getMainVBox().getLabelInfo().setText("BDD vide. Veuillez ajouter un etudiant");
					}
					raf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		root.getMainVBox().getBtnCo().getBtnSearch().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				root.getMainVBox().getLabelInfo().setText(null);
				RandomAccessFile raf;
				try {
					raf = new RandomAccessFile(dao.getDestinationFilePath(), "rw");
					if (raf.length()!=0) {
						String tfSurname, tfName, tfLocation, tfPromotion, tfYear;
						List<Student> studentList = new ArrayList<Student>();
						tfSurname=root.getMainVBox().getTxtFiCo().getTxtSurname().getText();
						tfName=root.getMainVBox().getTxtFiCo().getTxtName().getText();
						tfLocation=root.getMainVBox().getTxtFiCo().getTxtLocation().getText();
						tfPromotion=root.getMainVBox().getTxtFiCo().getTxtPromotion().getText();
						tfYear=root.getMainVBox().getTxtFiCo().getTxtYear().getText();
						if (tfSurname.isEmpty()==false && tfName.isEmpty()==true && 
								tfLocation.isEmpty()==true && tfPromotion.isEmpty()==true 
								&& tfYear.isEmpty()==true)
						{
							studentList = dao.searchBySurname(tfSurname);
							root.getTblvStudents().getObsStudent().clear();
							root.getTblvStudents().getObsStudent().addAll(studentList);
						}
						else
						{
							studentList = dao.multipleSearchMethod(tfSurname, 
									tfName, tfLocation, tfPromotion, tfYear, studentList);
							root.getTblvStudents().getObsStudent().clear();
							root.getTblvStudents().getObsStudent().addAll(studentList);
						}

					} else {
						root.getMainVBox().getLabelInfo().setText("BDD vide. Veuillez ajouter un etudiant");
					}
					raf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		root.getMainVBox().getBtnCo().getBtnErase().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				long cursor;
				if (isAdmin==true) {
					root.getMainVBox().getLabelInfo().setText(null);
					Student student = root.getTblvStudents().getTblvStudent().getSelectionModel().getSelectedItem();
					root.getTblvStudents().getObsStudent().remove(student);
					if ((student != null))
					{
						try {
							RandomAccessFile raf = new RandomAccessFile(dao.getDestinationFilePath(), "rw");
							cursor = dao.searchStudentPointer(student, raf, 0);
							if (cursor==0) {
								List<Student> studentList = new ArrayList<Student>();
								studentList = dao.browseNonOrganize(0, raf, studentList);
								raf.close();
								File fileToDelete = new File(dao.getDestinationFilePath());
								Files.delete(fileToDelete.toPath());
								raf = new RandomAccessFile(dao.getDestinationFilePath(), "rw");
								for (Student s : studentList) {
									if (!(s.equals(student))) {
										dao.addStudent(s, raf, 0, 0);
									}
								}
							} else {
								student = dao.extractAllField(cursor, raf);
								dao.findAndErase(student, cursor, raf);
							}
							raf.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					else {
						root.getMainVBox().getLabelInfo().setText("Veuillez sélectionner un stagiaire");
					}


				} else {
					root.getMainVBox().getLabelInfo().setText("Connexion à la session administrateur necessaire");
				}
			}
		});

		root.getMainVBox().getBtnCo().getBtnModify().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				long cursor;
				root.getMainVBox().getLabelInfo().setText(null);
				if (isAdmin==true) {
					Student selectedStudent = root.getTblvStudents().getTblvStudent().getSelectionModel().getSelectedItem();
					if ((selectedStudent != null))
					{
						if (root.getMainVBox().getTxtFiCo().getTxtSurname().getText().isEmpty()==false) {
							if (root.getMainVBox().getTxtFiCo().getTxtSurname().getText().length()<=dao.getMaxSurnameField()&&
									root.getMainVBox().getTxtFiCo().getTxtName().getText().length()<=dao.getMaxNameField()
									&&root.getMainVBox().getTxtFiCo().getTxtLocation().getText().length()<=dao.getMaxLocationField()&&
									root.getMainVBox().getTxtFiCo().getTxtPromotion().getText().length()<=dao.getMaxPromotionField()
									&&root.getMainVBox().getTxtFiCo().getTxtYear().getText().length()<=dao.getMaxYearField()) {
								RandomAccessFile raf;
								try {
									raf = new RandomAccessFile(dao.getDestinationFilePath(), "rw");
									if (raf.length()!=0) {
										selectedStudent = root.getTblvStudents().getTblvStudent().getSelectionModel().getSelectedItem();
										if (selectedStudent.getSurname().equals(root.getMainVBox().getTxtFiCo().getTxtSurname().getText())) {
											Student modifyStudent = dao.modifyStudentOtherThanSurname(selectedStudent, root.getMainVBox().getTxtFiCo().getTxtName().getText(), 
													root.getMainVBox().getTxtFiCo().getTxtLocation().getText(), 
													root.getMainVBox().getTxtFiCo().getTxtPromotion().getText(), 
													root.getMainVBox().getTxtFiCo().getTxtYear().getText());
											root.getTblvStudents().getObsStudent().clear();
											root.getTblvStudents().getObsStudent().add(modifyStudent);
										} else {
											cursor = dao.searchStudentPointer(selectedStudent, raf, 0);
											if (cursor==0) {
												List<Student> studentList = new ArrayList<Student>();
												studentList = dao.browseNonOrganize(0, raf, studentList);
												raf.close();
												File fileToDelete = new File(dao.getDestinationFilePath());
												Files.delete(fileToDelete.toPath());
												raf = new RandomAccessFile(dao.getDestinationFilePath(), "rw");
												for (Student s : studentList) {
													if (!(s.equals(selectedStudent))) {
														dao.addStudent(s, raf, 0, 0);
													}
												}
												Student modifiedStudent = dao.modifyStudentSurname(root.getMainVBox().getTxtFiCo().getTxtSurname().getText(), 
														root.getMainVBox().getTxtFiCo().getTxtName().getText(), 
														root.getMainVBox().getTxtFiCo().getTxtLocation().getText(), 
														root.getMainVBox().getTxtFiCo().getTxtPromotion().getText(), 
														root.getMainVBox().getTxtFiCo().getTxtYear().getText(), raf);
												root.getTblvStudents().getObsStudent().clear();
												root.getTblvStudents().getObsStudent().add(modifiedStudent);
											} else {
												selectedStudent = dao.extractAllField(cursor, raf);
												dao.findAndErase(selectedStudent, cursor, raf);
												Student modifiedStudent = dao.modifyStudentSurname(root.getMainVBox().getTxtFiCo().getTxtSurname().getText(), 
														root.getMainVBox().getTxtFiCo().getTxtName().getText(), 
														root.getMainVBox().getTxtFiCo().getTxtLocation().getText(), 
														root.getMainVBox().getTxtFiCo().getTxtPromotion().getText(), 
														root.getMainVBox().getTxtFiCo().getTxtYear().getText(), raf);
												root.getTblvStudents().getObsStudent().clear();
												root.getTblvStudents().getObsStudent().add(modifiedStudent);
											}
										}
									} else {
										root.getMainVBox().getLabelInfo().setText("BDD vide. Veuillez ajouter un etudiant");
									}
									raf.close();
								} catch (IOException e) {
									e.printStackTrace();
								}
							} else {
								root.getMainVBox().getLabelInfo().setText("Un des champs est trop grand");
							}
						} else {
							root.getMainVBox().getLabelInfo().setText("Veuillez entrer un nom de famille");
						}
					}
					else
					{
						root.getMainVBox().getLabelInfo().setText("Veuillez sélectionner un stagiaire");
					}
					
				} else {
					root.getMainVBox().getLabelInfo().setText("Connexion à la session administrateur necessaire");
				}
			}
		});

		root.getTblvStudents().getTblvStudent().getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Student>() {

			@Override
			public void changed(ObservableValue<? extends Student> observable, Student oldValue, Student newValue) {
				try {
					root.getMainVBox().getTxtFiCo().getTxtSurname().setText(newValue.getSurname());
					root.getMainVBox().getTxtFiCo().getTxtName().setText(newValue.getName());
					root.getMainVBox().getTxtFiCo().getTxtLocation().setText(newValue.getLocation());
					root.getMainVBox().getTxtFiCo().getTxtPromotion().setText(newValue.getPromotion());
					root.getMainVBox().getTxtFiCo().getTxtYear().setText(newValue.getYear());
				} catch (NullPointerException e) {
					/*
					 * Gestion de NullPointer Exception générée lors du refresh du TableView
					 */
				}
			}
		});

		root.getMainVBox().getBtnCo().getBtnAdd().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				root.getMainVBox().getLabelInfo().setText(null);
				try {
					RandomAccessFile raf = new RandomAccessFile(dao.getDestinationFilePath(), "rw");
					Student newStudent = new Student(dao.formatStudentSurname(root.getMainVBox().getTxtFiCo().getTxtSurname().getText()), 
							root.getMainVBox().getTxtFiCo().getTxtName().getText(), 
							root.getMainVBox().getTxtFiCo().getTxtLocation().getText(), 
							root.getMainVBox().getTxtFiCo().getTxtPromotion().getText(), 
							root.getMainVBox().getTxtFiCo().getTxtYear().getText());
					if (root.getMainVBox().getTxtFiCo().getTxtSurname().getText().isEmpty()==false) {
						if (newStudent.getSurname().length()<=dao.getMaxSurnameField()&&newStudent.getName().length()<=dao.getMaxNameField()
								&&newStudent.getLocation().length()<=dao.getMaxLocationField()&&newStudent.getPromotion().length()<=dao.getMaxPromotionField()
								&&newStudent.getYear().length()<=dao.getMaxYearField()) {
							dao.addStudent(newStudent, raf, 0, 0);
							root.getTblvStudents().getObsStudent().clear();;
							root.getTblvStudents().getObsStudent().add(newStudent);
						} else {
							root.getMainVBox().getLabelInfo().setText("Un des champs est trop grand");
						}
					} else {
						root.getMainVBox().getLabelInfo().setText("Veuillez entrer un nom de famille");
					}
					raf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		root.getMainVBox().getAdminCommand().getButtonAdminCommand().getConnection().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				root.getMainVBox().getLabelInfo().setText(null);
				String mdpInput = root.getMainVBox().getAdminCommand().getAdminInput().getTfMDP().getText();
				root.getMainVBox().getAdminCommand().getAdminInput().getTfMDP().setText(null);

				if (isAdmin==true) {
					root.getMainVBox().getLabelInfo().setText("Session administrateur deja connectée");
				} else {
					if (null==mdpInput || mdpInput.isEmpty()) {
						root.getMainVBox().getLabelInfo().setText("Veuillez taper un mot de passe");
					} else if (mdpInput.equals("origami")) {
						root.getMainVBox().getLabelInfo().setText("Vous etes connecté");
						isAdmin=true;
					} else {
						root.getMainVBox().getLabelInfo().setText("Mot de passe incorrect");
					}
				}
			}
		});

		root.getMainVBox().getAdminCommand().getButtonAdminCommand().getLogout().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				root.getMainVBox().getLabelInfo().setText(null);
				if (isAdmin==true) {
					isAdmin=false;
					root.getMainVBox().getLabelInfo().setText("Deconnexion de la session adiministrateur");
				} else {
					root.getMainVBox().getLabelInfo().setText("Session administrateur non utilisé");
				}
			}
		});

		/*
		 * Fonctionnalité bouton Réinitialiser
		 */


		root.getMainVBox().getTxtFiCo().getBtnClear().setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				root.getMainVBox().getTxtFiCo().getTxtSurname().setText("");
				root.getMainVBox().getTxtFiCo().getTxtName().setText("");
				root.getMainVBox().getTxtFiCo().getTxtLocation().setText("");
				root.getMainVBox().getTxtFiCo().getTxtPromotion().setText("");
				root.getMainVBox().getTxtFiCo().getTxtYear().setText("");
			}
		});

				root.getMainVBox().getBtnCo().getBtnExport().setOnAction(new EventHandler<ActionEvent>() {
		
					@Override
					public void handle(ActionEvent event) {
						export.exportation(root.getTblvStudents().getObsStudent());
						root.getMainVBox().getLabelInfo().setText("Fichier ExportList créé");
					}
				});
	}
}
