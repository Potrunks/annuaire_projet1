package fr.eql.ai110.group2.project1.formpanel;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class AdminInput extends HBox {

	private Label lblMDP = new Label("Mot de passe");
	private TextField tfMDP = new TextField();

	public AdminInput() {
		getChildren().addAll(lblMDP, tfMDP);
		setSpacing(10);
		setAlignment(Pos.CENTER);
	}

	public Label getLblMDP() {
		return lblMDP;
	}

	public void setLblMDP(Label lblMDP) {
		this.lblMDP = lblMDP;
	}

	public TextField getTfMDP() {
		return tfMDP;
	}

	public void setTfMDP(TextField tfMDP) {
		this.tfMDP = tfMDP;
	}


	
	
}
