package fr.eql.ai110.group2.project1.formpanel;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

public class ButtonAdminCommand extends HBox {

	private Button connection = new Button("Se connecter");
	private Button logout = new Button("Se deconnecter");
	
	public ButtonAdminCommand() {
		getChildren().addAll(connection, logout);
		setSpacing(10);
		setAlignment(Pos.CENTER);
	}

	public Button getConnection() {
		return connection;
	}

	public void setConnection(Button connection) {
		this.connection = connection;
	}

	public Button getLogout() {
		return logout;
	}

	public void setLogout(Button logout) {
		this.logout = logout;
	}
	
	
}
