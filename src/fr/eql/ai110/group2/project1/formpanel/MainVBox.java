package fr.eql.ai110.group2.project1.formpanel;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class MainVBox extends VBox {

	private ButtonCommand btnCo = new ButtonCommand();
	private TextFieldCommand txtFiCo = new TextFieldCommand();
	private FileChooserFirstLaunch fileChooFiLa = new FileChooserFirstLaunch();
	private BorderPaneDoc btnDoc = new BorderPaneDoc();
	private AdminCommand adminCommand = new AdminCommand();
	private Label labelInfo = new Label();
	private Button logoEQL = new Button();



	public MainVBox() {
		Image searchIcon = new Image(getClass().getResourceAsStream("./img/LogoEQL4redim1.jpg"));

		logoEQL.setPrefSize(350, 200);
		logoEQL.setGraphic(new ImageView(searchIcon));
		logoEQL.setId("logo");

		getChildren().addAll(btnDoc, logoEQL, labelInfo, txtFiCo, btnCo, adminCommand);

		labelInfo.setTextFill(Color.RED);
		labelInfo.setFont(Font.font(15));

		setPrefSize(400, 800);
		setAlignment(Pos.CENTER);
		//		setStyle("-fx-background-color: linear-gradient(#80FFDB, #5390D9)");
		setStyle("-fx-background-color: linear-gradient(#6FDEBE, #5390D9)");
		//		setStyle("-fx-background-color: linear-gradient(#ff8a00, #e52e71)");
		setPadding(new Insets(20));
		setSpacing(25);
	}

	public ButtonCommand getBtnCo() {
		return btnCo;
	}

	public void setBtnCo(ButtonCommand btnCo) {
		this.btnCo = btnCo;
	}

	public TextFieldCommand getTxtFiCo() {
		return txtFiCo;
	}

	public void setTxtFiCo(TextFieldCommand txtFiCo) {
		this.txtFiCo = txtFiCo;
	}

	public FileChooserFirstLaunch getFileChooFiLa() {
		return fileChooFiLa;
	}

	public void setFileChooFiLa(FileChooserFirstLaunch fileChooFiLa) {
		this.fileChooFiLa = fileChooFiLa;
	}

	public BorderPaneDoc getBtnDoc() {
		return btnDoc;
	}

	public void setBtnDoc(BorderPaneDoc btnDoc) {
		this.btnDoc = btnDoc;
	}

	public AdminCommand getAdminCommand() {
		return adminCommand;
	}

	public void setAdminCommand(AdminCommand adminCommand) {
		this.adminCommand = adminCommand;
	}

	public Label getLabelInfo() {
		return labelInfo;
	}

	public void setLabelInfo(Label labelInfo) {
		this.labelInfo = labelInfo;
	}



}
