package fr.eql.ai110.group2.project1.formpanel;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class TextFieldCommand extends GridPane {

	private Label lblSurname = new Label("Nom");
	private TextField txtSurname = new TextField();
	private Label lblName = new Label("Prénom");
	private TextField txtName = new TextField();
	private Label lblLocation = new Label("Département");
	private TextField txtLocation = new TextField();
	private Label lblPromotion = new Label("Promotion");
	private TextField txtPromotion = new TextField();
	private Label lblYear = new Label("Année");
	private TextField txtYear = new TextField();
	private Button btnClear= new Button("Réinitialiser");
	
	public TextFieldCommand() {
		addRow(0, lblSurname,txtSurname);
		addRow(1, lblName,txtName);
		addRow(2, lblLocation,txtLocation);
		addRow(3, lblPromotion,txtPromotion);
		addRow(4, lblYear,txtYear, btnClear);
		
		setHgap(20);
		setVgap(10);
		setAlignment(Pos.TOP_CENTER);
	}

	public Label getLblSurname() {
		return lblSurname;
	}

	public void setLblSurname(Label lblSurname) {
		this.lblSurname = lblSurname;
	}

	public TextField getTxtSurname() {
		return txtSurname;
	}

	public void setTxtSurname(TextField txtSurname) {
		this.txtSurname = txtSurname;
	}

	public Label getLblName() {
		return lblName;
	}

	public void setLblName(Label lblName) {
		this.lblName = lblName;
	}

	public TextField getTxtName() {
		return txtName;
	}

	public void setTxtName(TextField txtName) {
		this.txtName = txtName;
	}

	public Label getLblLocation() {
		return lblLocation;
	}

	public void setLblLocation(Label lblLocation) {
		this.lblLocation = lblLocation;
	}

	public TextField getTxtLocation() {
		return txtLocation;
	}

	public void setTxtLocation(TextField txtLocation) {
		this.txtLocation = txtLocation;
	}

	public Label getLblPromotion() {
		return lblPromotion;
	}

	public void setLblPromotion(Label lblPromotion) {
		this.lblPromotion = lblPromotion;
	}

	public TextField getTxtPromotion() {
		return txtPromotion;
	}

	public void setTxtPromotion(TextField txtPromotion) {
		this.txtPromotion = txtPromotion;
	}

	public Label getLblYear() {
		return lblYear;
	}

	public void setLblYear(Label lblYear) {
		this.lblYear = lblYear;
	}

	public TextField getTxtYear() {
		return txtYear;
	}

	public void setTxtYear(TextField txtYear) {
		this.txtYear = txtYear;		
	}
	
	public Button getBtnClear() {
		return btnClear;
	}

	public void setBtnClear(Button btnClear) {
		this.btnClear = btnClear;
	}	
}
