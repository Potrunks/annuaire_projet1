package fr.eql.ai110.group2.project1.formpanel;

import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

public class BorderPaneDoc extends BorderPane{
	private Button btnDoc = new Button("Aide");

	public BorderPaneDoc() {
		setLeft(btnDoc);
	}

	public Button getBtnDoc() {
		return btnDoc;
	}

	public void setBtnDoc(Button btnDoc) {
		this.btnDoc = btnDoc;
	}
	
	}
