package fr.eql.ai110.group2.project1.formpanel;

import javafx.geometry.Pos;
import javafx.scene.layout.VBox;

public class AdminCommand extends VBox {

	private AdminInput adminInput = new AdminInput();
	private ButtonAdminCommand buttonAdminCommand = new ButtonAdminCommand();
	
	public AdminCommand() {
		getChildren().addAll(adminInput, buttonAdminCommand);
		setAlignment(Pos.CENTER);
		setSpacing(10);
	}

	public AdminInput getAdminInput() {
		return adminInput;
	}

	public void setAdminInput(AdminInput adminInput) {
		this.adminInput = adminInput;
	}

	public ButtonAdminCommand getButtonAdminCommand() {
		return buttonAdminCommand;
	}

	public void setButtonAdminCommand(ButtonAdminCommand buttonAdminCommand) {
		this.buttonAdminCommand = buttonAdminCommand;
	}
	
	
}
