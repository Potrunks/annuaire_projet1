package fr.eql.ai110.group2.project1.formpanel;

import fr.eql.ai110.group2.project1.tableview.TableViewStudents;
import javafx.scene.layout.BorderPane;

public class MainPanel extends BorderPane {

	private MainVBox mainVBox = new MainVBox();
	private TableViewStudents tblvStudents = new TableViewStudents();
	
	public MainPanel() {
		setLeft(mainVBox);
		setCenter(tblvStudents);
	}

	public MainVBox getMainVBox() {
		return mainVBox;
	}

	public void setMainVBox(MainVBox mainVBox) {
		this.mainVBox = mainVBox;
	}

	public TableViewStudents getTblvStudents() {
		return tblvStudents;
	}

	public void setTblvStudents(TableViewStudents tblvStudents) {
		this.tblvStudents = tblvStudents;
	}
	
	
	
}
