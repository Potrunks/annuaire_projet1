package fr.eql.ai110.group2.project1.formpanel;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

public class FileChooserFirstLaunch extends BorderPane {

	private Button open = new Button("Importer");
	private Label label = new Label();

	public FileChooserFirstLaunch() {
		setCenter(open);
		open.setPrefSize(100, 60);
	}

	public Button getOpen() {
		return open;
	}

	public void setOpen(Button open) {
		this.open = open;
	}

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}
	
	
	
}
