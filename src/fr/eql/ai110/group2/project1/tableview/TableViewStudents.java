package fr.eql.ai110.group2.project1.tableview;

import javafx.scene.layout.AnchorPane;
import fr.eql.ai110.group2.project1.database.Student;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class TableViewStudents extends AnchorPane {

	private TableView<Student> tblvStudent;
	private ObservableList<Student> obsStudent = FXCollections.observableArrayList();

	public TableViewStudents() {
		
		tblvStudent = new TableView<Student>(obsStudent);

		TableColumn<Student, String> colSurname = new TableColumn<>("Nom");
		colSurname.setCellValueFactory(new PropertyValueFactory<>("surname"));
		TableColumn<Student, String> colName = new TableColumn<>("Prénom");
		colName.setCellValueFactory(new PropertyValueFactory<>("name"));
		TableColumn<Student, String> colLocation = new TableColumn<>("Département");
		colLocation.setCellValueFactory(new PropertyValueFactory<>("location"));
		TableColumn<Student, String> colPromotion = new TableColumn<>("Promotion");
		colPromotion.setCellValueFactory(new PropertyValueFactory<>("promotion"));
		TableColumn<Student, String> colYear = new TableColumn<>("Année");
		colYear.setCellValueFactory(new PropertyValueFactory<>("year"));

		tblvStudent.getColumns().addAll(colSurname, colName, colLocation, colPromotion, colYear);
		tblvStudent.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		getChildren().add(tblvStudent);
		setPrefSize(600, 800);

		AnchorPane.setTopAnchor(tblvStudent, 5.);
		AnchorPane.setLeftAnchor(tblvStudent, 5.);
		AnchorPane.setRightAnchor(tblvStudent, 5.);
		AnchorPane.setBottomAnchor(tblvStudent, 5.);
	}

	public ObservableList<Student> getObsStudent() {
		return obsStudent;
	}

	public void setObsStudent(ObservableList<Student> obsStudent) {
		this.obsStudent = obsStudent;
	}

	public TableView<Student> getTblvStudent() {
		return tblvStudent;
	}

	public void setTblvStudent(TableView<Student> tblvStudent) {
		this.tblvStudent = tblvStudent;
	}
	
}
