package fr.eql.ai110.group2.project1.database;


import java.io.FileOutputStream;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import javafx.collections.ObservableList;

public class Export {	
	/*
	 * Création de convertisseur itext
	 */	
	private String DestinationFilePath = "./ExportList.pdf";
	
	public void exportation(ObservableList<Student> obsStudent) {
		Document document = new Document(PageSize.A4,30,30,25,25);
		try
		{
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(DestinationFilePath));
		    document.open();
		    
		    PdfPTable table = new PdfPTable(5);
		    table.setWidthPercentage(100); //Width 100%
		    table.setSpacingBefore(10f); //Space before table
		    table.setSpacingAfter(10f); //Space after table
		    
		    float[] columnWidths = {1.6f, 1.2f, 0.5f,0.8f, 0.5f};
		    table.setWidths(columnWidths);
		
		    PdfPCell cell1 = new PdfPCell(new Paragraph("Nom"));		 
		    cell1.setPadding(10);
		    cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		    cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		    PdfPCell cell2 = new PdfPCell(new Paragraph("Prénom"));
		    cell2.setPadding(10);
		    cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		    cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		    PdfPCell cell3 = new PdfPCell(new Paragraph("Dépt."));		  
		    cell3.setPadding(10);
		    cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
		    cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		    PdfPCell cell4 = new PdfPCell(new Paragraph("Promotion"));		   
		    cell4.setPadding(10);
		    cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
		    cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
		    
		    PdfPCell cell5 = new PdfPCell(new Paragraph("Année"));		   
		    cell5.setPadding(10);
		    cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
		    cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		    table.addCell(cell1);
		    table.addCell(cell2);
		    table.addCell(cell3);
		    table.addCell(cell4);
		    table.addCell(cell5);
		
		    document.add(table);
		   
		    for (Student student : obsStudent)
		    {
		    table = new PdfPTable(5);
		    table.setWidthPercentage(100); //Width 100%
		    table.setSpacingBefore(10f); //Space before table
		    table.setSpacingAfter(10f); //Space after table
		    
		    float[] columnWidths1 = {1.6f, 1.2f, 0.5f,0.8f, 0.5f};
		    table.setWidths(columnWidths1);
		
		    cell1 = new PdfPCell(new Paragraph(student.getSurname()));		 
		    cell1.setPadding(10);
		    cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		    cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		    cell2 = new PdfPCell(new Paragraph(student.getName()));
		    cell2.setPadding(10);
		    cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		    cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		    cell3 = new PdfPCell(new Paragraph(student.getLocation()));		  
		    cell3.setPadding(10);
		    cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
		    cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		    cell4 = new PdfPCell(new Paragraph(student.getPromotion()));		   
		    cell4.setPadding(10);
		    cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
		    cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
		    
		    cell5 = new PdfPCell(new Paragraph(student.getYear()));		   
		    cell5.setPadding(10);
		    cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
		    cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		    table.addCell(cell1);
		    table.addCell(cell2);
		    table.addCell(cell3);
		    table.addCell(cell4);
		    table.addCell(cell5);
		
		    document.add(table);		    
		   
		    }
		 
		    document.close();	   
		  	
		} catch (Exception e) {
		    e.printStackTrace();
		}
	}
}
