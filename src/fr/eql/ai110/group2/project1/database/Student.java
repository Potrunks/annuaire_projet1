package fr.eql.ai110.group2.project1.database;

import java.util.Objects;

public class Student {
	
	private String surname, name, location, promotion, year, left, right;
	private int parent;

	public Student(String surname, String name, String location, String promotion, String year) {
		this.surname = surname;
		this.name = name;
		this.location = location;
		this.promotion = promotion;
		this.year = year;
	}
	
	public Student(String surname, String left, String right, int parent) {
		this.surname = surname;
		this.left = left;
		this.right = right;
		this.parent = parent;
	}

	public String getLeft() {
		return left;
	}

	public void setLeft(String left) {
		this.left = left;
	}

	public String getRight() {
		return right;
	}

	public void setRight(String right) {
		this.right = right;
	}

	public int getParent() {
		return parent;
	}

	public void setParent(int parent) {
		this.parent = parent;
	}

	public Student() {
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPromotion() {
		return promotion;
	}

	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Override
	public int hashCode() {
		return Objects.hash(location, name, promotion, surname, year);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		return Objects.equals(location, other.location) && Objects.equals(name, other.name)
				&& Objects.equals(promotion, other.promotion) && Objects.equals(surname, other.surname)
				&& Objects.equals(year, other.year);
	}
}

