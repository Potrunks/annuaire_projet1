package fr.eql.ai110.group2.project1.database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import fr.eql.ai110.group2.project1.formpanel.MainPanel;
import javafx.event.ActionEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class DAO {

	private static final int MAX_SURNAME_FIELD = 30, MAX_NAME_FIELD = 30, MAX_LOCATION_FIELD = 4, MAX_PROMOTION_FIELD = 15, MAX_YEAR_FIELD = 5, MAX_CHILD_FIELD = 19;
	private static final int MAX_FIELD = MAX_SURNAME_FIELD + MAX_NAME_FIELD + MAX_LOCATION_FIELD + MAX_PROMOTION_FIELD + MAX_YEAR_FIELD + MAX_CHILD_FIELD + MAX_CHILD_FIELD;
	private static String DestinationFilePath = "./stagiaireRAF.bin";

	/**
	 * Methode appelé depuis le Main App dans fr.perso.potrunks.annuaire.view
	 * Lorsque l'utilisateur clique sur le bouton "Ouvrir", il aura la possibilité d'ouvrir un fichier contenant la liste
	 * de stagiaire à transformer en annuaire mais uniquement si l'annuaire n'a jamais été créé.
	 * @param event
	 * @param primaryStage
	 * @param root
	 */
	public void launch(ActionEvent event, Stage stage, MainPanel root) {
		try {
			RandomAccessFile raf = new RandomAccessFile(DestinationFilePath, "rw");
			FileChooser fileChooser = new FileChooser();
			File initDir = new File(".");
			fileChooser.setInitialDirectory(initDir);
			File choosenFile = fileChooser.showOpenDialog(stage.getOwner());
			float startTime = System.nanoTime();
			List<Student> studentList = readSourceFile(choosenFile);
			for (Student s : studentList) {
				addStudent(s, raf, 0, 0);
			}
			raf.close();
			float endTime = System.nanoTime();
			root.getMainVBox().getLabelInfo().setText("Création de l'annuaire en "+((endTime-startTime)/(1000000000))+" secondes");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Permet d'ecrire un stagiaire dans le RAF
	 * @param s
	 * @param raf
	 */
	private void writeStudent(Student s, RandomAccessFile raf) {
		s.setSurname(formatStudentSurname(s.getSurname()));
		writeFieldByField(s.getSurname(), MAX_SURNAME_FIELD, raf);
		writeFieldByField(s.getName(), MAX_NAME_FIELD, raf);
		writeFieldByField(s.getLocation(), MAX_LOCATION_FIELD, raf);
		writeFieldByField(s.getPromotion(), MAX_PROMOTION_FIELD, raf);
		writeFieldByField(s.getYear(), MAX_YEAR_FIELD, raf);
		try {
			raf.writeBytes("0");
			for (int i=0; i<MAX_CHILD_FIELD-1; i++ ) {
				raf.writeBytes(" ");
			}
			raf.writeBytes("0");
			for (int i=0; i<MAX_CHILD_FIELD-1; i++ ) {
				raf.writeBytes(" ");
			}
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	/**
	 * Permet d'ecrire juste un des argument du stagiaire (Nom ou prénom ou adresse, etc...)
	 * @param get
	 * @param max
	 * @param raf
	 */
	private void writeFieldByField(String get, int max, RandomAccessFile raf) {
		if (get.length()==max) {
			try {
				raf.writeBytes(get);
			} catch (IOException e) {

				e.printStackTrace();
			}
		} else {
			try {
				raf.writeBytes(get);
			} catch (IOException e) {

				e.printStackTrace();
			}
			for (int i=0; i<max-get.length(); i++) {
				try {
					raf.writeBytes(" ");
				} catch (IOException e) {

					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Cette methode permet d'ajouter un stagiaire dans le RAF avec la creation d'un systeme d'arborescence dont chaque 
	 * stagiaire contiendra l'information s'il posséde un ou 2 noeuds "fils" appelé "left" et "right".
	 * Chaque nom de famille du stagiaire se verra comparer avec des stagiaires dans le RAF pour pouvoir situé ou l'ecrire et baliser
	 * avec un index "left" ou "right" sur son noeud "parent".
	 * Par convention, un nom de famille plus grand se verra affecter à un "left" disponible et sinon à un "right" disponible
	 * @param s
	 * @param raf
	 * @param cursor
	 * @param parent
	 */
	public void addStudent(Student s, RandomAccessFile raf, int cursor, int parent) {
		try {
			raf.seek(cursor);
			if (raf.length()!=0) {
				Student rafStudent = extractAllField(cursor, raf);
				String rafSurname = rafStudent.getSurname();
				int rafLeft = Integer.parseInt(rafStudent.getLeft());
				int rafRight = Integer.parseInt(rafStudent.getRight());
				if (s.getSurname().compareTo(rafSurname)<0) {
					if (rafLeft!=0) {
						raf.seek(cursor);
						rafStudent.setParent(cursor);
						int rafParent = rafStudent.getParent();
						addStudent(s, raf, rafLeft, rafParent);
					} else {
						raf.seek(cursor);
						rafStudent.setParent(cursor);
						int rafParent = rafStudent.getParent();
						long child = raf.length();
						raf.seek(child);
						writeStudent(s, raf);
						updateParent(s, rafParent, child, raf, rafSurname);
					}
				} else {
					if (rafRight!=0) {
						raf.seek(cursor);
						rafStudent.setParent(cursor);
						int rafParent = rafStudent.getParent();
						addStudent(s, raf, rafRight, rafParent);
					} else {
						raf.seek(cursor);
						rafStudent.setParent(cursor);
						int rafParent = rafStudent.getParent();
						long child = raf.length();
						raf.seek(child);
						writeStudent(s, raf);
						updateParent(s, rafParent, child, raf, rafSurname);
					}
				}
			} else {
				writeStudent(s, raf);
			}
		} catch (IOException e1) {

			e1.printStackTrace();
		}
	}

	/**
	 * Permet d'extraire tous les champs d'un stagiaire depuis le RAF et instancier stagiaire
	 * @param cursor
	 * @param raf
	 * @return
	 */
	public Student extractAllField(long cursor, RandomAccessFile raf) {
		Student st = new Student();
		try {
			raf.seek(cursor);
			st.setSurname(extractField(MAX_SURNAME_FIELD, raf));
			st.setName(extractField(MAX_NAME_FIELD, raf));
			st.setLocation(extractField(MAX_LOCATION_FIELD, raf));
			st.setPromotion(extractField(MAX_PROMOTION_FIELD, raf));
			st.setYear(extractField(MAX_YEAR_FIELD, raf));
			st.setLeft(extractField(MAX_CHILD_FIELD, raf));
			st.setRight(extractField(MAX_CHILD_FIELD, raf));
		} catch (IOException e) {

			e.printStackTrace();
		}
		return st;
	}

	/**
	 * Permet d'extraire juste un champ du stagiaire du RAF
	 * @param max
	 * @param raf
	 * @return
	 */
	private String extractField(int max, RandomAccessFile raf) {
		String line = "";
		try {
			byte[] b = new byte[max];
			raf.read(b, 0, max);
			line = new String(b, StandardCharsets.ISO_8859_1);
			line = line.trim();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return line;
	}

	/**
	 * Permet de mettre a jour un parent quand il se voit attribuer un fils (que se soit droite ou gauche)
	 * @param s
	 * @param parent
	 * @param child
	 * @param raf
	 * @param rafSurname
	 */
	private void updateParent(Student s, long parent, long child, RandomAccessFile raf, String rafSurname) {
		try {
			raf.seek(parent);
			if (s.getSurname().compareTo(rafSurname)<0) {
				raf.seek(parent+MAX_FIELD-(MAX_CHILD_FIELD*2));
				writeFieldByField(Long.toString(child), MAX_CHILD_FIELD, raf);
			} else {
				raf.seek(parent+MAX_FIELD-(MAX_CHILD_FIELD));
				writeFieldByField(Long.toString(child), MAX_CHILD_FIELD, raf);
			}
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	/**
	 * Permet de lire le fichier source du User et d'extraire les champs pour en faire une liste de stagaire
	 * destiné à etre ecrit dans le RAF pour creer l'annuaire (principalement utilisé qu'une seule fois lors du premier
	 * lancement de l'application)
	 * @param choosenFile
	 * @return
	 */
	private List<Student> readSourceFile(File choosenFile) {
		List<Student> studentList = new ArrayList<Student>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(choosenFile));
			while (br.ready()) {
				String surname = br.readLine().trim().toUpperCase();
				String nicksurname = br.readLine().trim();
				String location = br.readLine().trim();
				String promotion = br.readLine().trim();
				String year = br.readLine().trim();
				Student student = new Student(surname, nicksurname, location, promotion, year);
				studentList.add(student);
				br.readLine();
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return studentList;
	}

	/**
	 * Permet de parcourir tout le RAF pour chercher les stagiaire par ordre alphabetique de leur nom de famille
	 * @param cursor
	 * @return
	 */
	public List<Student> allBrowse(long cursor) {
		List<Student> studentList = new ArrayList<Student>();
		try {
			RandomAccessFile raf = new RandomAccessFile(DestinationFilePath, "rw");
			studentList = browse(cursor, raf, studentList);
			raf.close();
		} catch (IOException e) {
			System.out.println("Fichier non trouvé");
			e.printStackTrace();
		}
		return studentList;
	}

	/**
	 * Permet de parcourir un stagiaire de l'arbre binaire. Si le stagiaire n'est pas le plus petit par ordre alphabetique,
	 * il fait un lancement de la même methode jusuqu'a trouvé le plus petit par ordre alphabetique. Une fois qu'il a été trouvé
	 * il va lancer cette meme methode mais sur le noeud qui est juste à la suite du plus petit afin de trouvé le second plus
	 * petit par ordre alphabetique
	 * @param cursor
	 * @param raf
	 * @param studentList
	 * @return
	 */
	private List<Student> browse(long cursor, RandomAccessFile raf, List<Student> studentList) {
		try {
			raf.seek(cursor+MAX_FIELD-(MAX_CHILD_FIELD*2));
			long rafLeft = Long.parseLong(extractField(MAX_CHILD_FIELD, raf));
			if (rafLeft!=0) {
				browse(rafLeft, raf, studentList);
				studentList.add(extractAllField(cursor, raf));
				raf.seek(cursor+MAX_FIELD-MAX_CHILD_FIELD);
				long rafRight = Long.parseLong(extractField(MAX_CHILD_FIELD, raf));
				if (rafRight!=0) {
					browse(rafRight, raf, studentList);
				}
			} else {
				studentList.add(extractAllField(cursor, raf));
				raf.seek(cursor+MAX_FIELD-MAX_CHILD_FIELD);
				long rafRight = Long.parseLong(extractField(MAX_CHILD_FIELD, raf));
				if (rafRight!=0) {
					browse(rafRight, raf, studentList);
				}
			}
		} catch (IOException e) {
			System.out.println("Impossible d'atteindre le curseur");
			System.out.println("cursor : "+cursor);
			System.out.println("maxField : "+MAX_FIELD);
			System.out.println("maxChildField : "+MAX_CHILD_FIELD);
			e.printStackTrace();
		}
		return studentList;
	}

	/**
	 * Permet, en cas de supression d'un noeud de l'arbre, de trouvé un emplacement libre pour ecrire les eventuels emplacement
	 * des noeuds issu du noeud supprimé.
	 * @param cursor
	 * @param orphan
	 * @param rafSurnameOrphan
	 * @param raf
	 */
	private void moveOrphan(long cursor, long orphan, String rafSurnameOrphan, RandomAccessFile raf) {
		try {
			raf.seek(cursor);
			String rafSurname = extractField(MAX_NAME_FIELD, raf);
			if (rafSurnameOrphan.compareTo(rafSurname)<0) {
				raf.seek(cursor+MAX_FIELD-(MAX_CHILD_FIELD*2));
				long rafLeft = Long.parseLong(extractField(MAX_CHILD_FIELD, raf));
				if (rafLeft!=0) {
					moveOrphan(rafLeft, orphan, rafSurnameOrphan, raf);
				} else {
					raf.seek(cursor+MAX_FIELD-(MAX_CHILD_FIELD*2));
					writeFieldByField(Long.toString(orphan), MAX_CHILD_FIELD, raf);
				}
			} else {
				raf.seek(cursor+MAX_FIELD-MAX_CHILD_FIELD);
				long rafRight = Long.parseLong(extractField(MAX_CHILD_FIELD, raf));
				if (rafRight!=0) {
					moveOrphan(rafRight, orphan, rafSurnameOrphan, raf);
				} else {
					raf.seek(cursor+MAX_FIELD-MAX_CHILD_FIELD);
					writeFieldByField(Long.toString(orphan), MAX_CHILD_FIELD, raf);
				}
			}
		} catch (IOException e) {
			System.out.println("Probleme");
			System.out.println("Curseur : "+cursor);
			System.out.println("Orphan : "+orphan);
			System.out.println("rafSurname : "+rafSurnameOrphan);
			e.printStackTrace();
		}
	}

	public void findAndErase(Student studentToErase, long cursor, RandomAccessFile raf)
	{
		try
		{
			searchParentToUpdate(studentToErase, 0, cursor, raf);
			long orphanLeft = Integer.parseInt(studentToErase.getLeft());
			Student stOrphanLeft = extractAllField(orphanLeft, raf);
			long orphanRight = Integer.parseInt(studentToErase.getRight());
			Student stOrphanRight = extractAllField(orphanRight, raf);

			raf.seek(cursor);

			writeFieldByField(" ", MAX_SURNAME_FIELD, raf);
			writeFieldByField(" ", MAX_NAME_FIELD, raf);
			writeFieldByField(" ", MAX_LOCATION_FIELD, raf);
			writeFieldByField(" ", MAX_PROMOTION_FIELD, raf);
			writeFieldByField(" ", MAX_YEAR_FIELD, raf);
			writeFieldByField(" ", MAX_CHILD_FIELD, raf);
			writeFieldByField(" ", MAX_CHILD_FIELD, raf);

			moveOrphan(0, orphanLeft, stOrphanLeft.getSurname(), raf);
			moveOrphan(0, orphanRight, stOrphanRight.getSurname(), raf);

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}		
	}

	private void searchParentToUpdate(Student student, long cursor, long studentPointer, RandomAccessFile raf) {

		Student rafStudent = new Student();
		try
		{
			raf.seek(cursor);
			rafStudent = extractAllField(cursor, raf);	

			if (rafStudent.getLeft().equals(Long.toString(studentPointer)))
			{
				raf.seek(cursor);
				raf.seek(cursor + MAX_FIELD - (MAX_CHILD_FIELD*2));
				writeFieldByField("0", MAX_CHILD_FIELD, raf);
			}
			else if (rafStudent.getRight().equals(Long.toString(studentPointer)))
			{
				raf.seek(cursor);
				raf.seek(cursor + MAX_FIELD - (MAX_CHILD_FIELD));
				writeFieldByField("0", MAX_CHILD_FIELD, raf);

			} else
			{
				if (student.getSurname().compareTo(rafStudent.getSurname())<0 
						&& Integer.parseInt(rafStudent.getLeft())!=0)
				{
					searchParentToUpdate(student, Long.parseLong(rafStudent.getLeft()), 
							studentPointer, raf);
				} else
				{
					searchParentToUpdate(student, Long.parseLong(rafStudent.getRight()), 
							studentPointer, raf);
				}
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}			
	}

	public List<Student> multipleSearchMethod(String tfSurname, String tfName, String tfLocation, String tfPromotion, String tfYear, List<Student> studentList) {
		List<Student> studentListTemp = new ArrayList<Student>();
		studentListTemp = allBrowse(0);
		tfSurname = formatStudentSurname(tfSurname);
		tfName = formatStudentSurname(tfName);
		tfLocation = formatStudentSurname(tfLocation);
		tfPromotion = formatStudentSurname(tfPromotion);
		for (Student s : studentListTemp) {
			String formatSurname = formatStudentSurname(s.getSurname());
			String formatName = formatStudentSurname(s.getName());
			String formatLocation = formatStudentSurname(s.getLocation());
			String formatPromotion = formatStudentSurname(s.getPromotion());
			if ((formatSurname.equals(tfSurname))||tfSurname.isEmpty()==true) {
				if ((formatName.equals(tfName))||tfName.isEmpty()==true) {
					if ((formatLocation.equals(tfLocation))||tfLocation.isEmpty()==true) {
						if ((formatPromotion.equals(tfPromotion))||tfPromotion.isEmpty()==true) {
							if ((s.getYear().equals(tfYear))||tfYear.isEmpty()==true) {
								studentList.add(s);
							}
						}
					}
				}
			}
		}
		return studentList;
	}

	public List<Student> searchBySurname(String surname)
	{
		List<Student> searchedStudentList =new ArrayList<Student>();
		RandomAccessFile raf;

		try
		{
			raf = new RandomAccessFile(DestinationFilePath, "r");
			searchStudent(surname, raf, 0, searchedStudentList);
			raf.close();	
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return searchedStudentList;
	}
	
 	private List<Student> searchStudent(String surname, RandomAccessFile raf, int cursor, List<Student> searchedStudentList)
 	{	
 		Student s = new Student();
	 	String searchedSurname = formatStudentSurname(surname);

 		try {
			raf.seek(cursor);
			s = extractAllField(cursor, raf);	
			String rafSurname = s.getSurname();
			int rafLeft = Integer.parseInt(s.getLeft());
			int rafRight = Integer.parseInt(s.getRight());

			if (searchedSurname.compareTo(rafSurname) <0 && rafLeft !=0)
			{
				searchStudent(searchedSurname, raf, rafLeft, searchedStudentList);
			}
			else if (searchedSurname.compareTo(rafSurname) >0 && rafRight !=0)
			{
				searchStudent(searchedSurname, raf, rafRight, searchedStudentList);
			}
			else if (searchedSurname.compareTo(rafSurname) ==0)
			{
				searchedStudentList.add(s);
				if (rafRight !=0)
				{
					searchStudent(searchedSurname, raf, rafRight, searchedStudentList);
				}
			}
			else
			{

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return searchedStudentList;
	}

	public long searchStudentPointer(Student selectedStudent, RandomAccessFile raf, int cursor)
	{	
		long rafPointer=0;
		Student rafStudent = new Student();
		try {
			raf.seek(cursor);
			rafStudent = extractAllField(cursor, raf);
			if (rafStudent.equals(selectedStudent)) {
				raf.seek(cursor);
				rafPointer = raf.getFilePointer();
			} else {
				if (selectedStudent.getSurname().compareTo(rafStudent.getSurname())<0 && Integer.parseInt(rafStudent.getLeft())!=0) {
					rafPointer = searchStudentPointer(selectedStudent, raf, Integer.parseInt(rafStudent.getLeft()));
				} else {
					rafPointer = searchStudentPointer(selectedStudent, raf, Integer.parseInt(rafStudent.getRight()));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return rafPointer;
	}

	public String formatStudentSurname(String surname)
	{
		String result = surname;

		result = result.trim();
		result = result.replace('à', 'a');
		result = result.replace('é', 'e');
		result = result.replace('è', 'e');
		result = result.replace('ê', 'e');
		result = result.replace('-', ' ');
		result = result.toUpperCase();

		return result;
	}
	
	public Student modifyStudentSurname(String tfSurname, String tfName, String tfLocation, String tfPromotion, String tfYear, RandomAccessFile raf)
	{
		Student modifiedStudent = new Student();

		modifiedStudent.setSurname(tfSurname);
		modifiedStudent.setName(tfName);
		modifiedStudent.setLocation(tfLocation);
		modifiedStudent.setPromotion(tfPromotion);
		modifiedStudent.setYear(tfYear);
		
		addStudent(modifiedStudent, raf, 0, 0);
		return modifiedStudent;
	}
	
	public Student modifyStudentOtherThanSurname(Student selectedStudent, String tfName, String tfLocation, String tfPromotion, String tfYear) {
		Student modifyStudent = null;
		try {
			RandomAccessFile raf = new RandomAccessFile(DestinationFilePath, "rw");
			raf.seek(searchStudentPointer(selectedStudent, raf, 0)+MAX_SURNAME_FIELD);
			writeFieldByField(tfName, MAX_NAME_FIELD, raf);
			writeFieldByField(tfLocation, MAX_LOCATION_FIELD, raf);
			writeFieldByField(tfPromotion, MAX_PROMOTION_FIELD, raf);
			writeFieldByField(tfYear, MAX_YEAR_FIELD, raf);
			modifyStudent = new Student(selectedStudent.getSurname(), tfName, tfLocation, tfPromotion, tfYear);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return modifyStudent;
	}

	public List<Student> browseNonOrganize(long cursor, RandomAccessFile raf, List<Student> studentList) {
		try {
			raf.seek(cursor);
			studentList.add(extractAllField(cursor, raf));
			raf.seek(cursor+MAX_FIELD-(MAX_CHILD_FIELD*2));
			long rafLeft = Long.parseLong(extractField(MAX_CHILD_FIELD, raf));
			long rafRight = Long.parseLong(extractField(MAX_CHILD_FIELD, raf));
			if (rafLeft!=0) {
				browseNonOrganize(rafLeft, raf, studentList);
			}
			if (rafRight!=0) {
				browseNonOrganize(rafRight, raf, studentList);
			}
		} catch (IOException e) {
			System.out.println("Impossible d'atteindre le curseur");
			System.out.println("cursor : "+cursor);
			System.out.println("maxField : "+MAX_FIELD);
			System.out.println("maxChildField : "+MAX_CHILD_FIELD);
			e.printStackTrace();
		}
		return studentList;
	}

	public static int getMaxSurnameField() {
		return MAX_SURNAME_FIELD;
	}

	public static int getMaxNameField() {
		return MAX_NAME_FIELD;
	}

	public static int getMaxLocationField() {
		return MAX_LOCATION_FIELD;
	}

	public static int getMaxPromotionField() {
		return MAX_PROMOTION_FIELD;
	}

	public static int getMaxYearField() {
		return MAX_YEAR_FIELD;
	}

	public static int getMaxChildField() {
		return MAX_CHILD_FIELD;
	}

	public static int getMaxField() {
		return MAX_FIELD;
	}

	public static String getDestinationFilePath() {
		return DestinationFilePath;
	}

	public static void setDestinationFilePath(String destinationFilePath) {
		DestinationFilePath = destinationFilePath;
	}
}
